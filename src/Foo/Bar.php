<?php

namespace Melq\PhpTesting\Foo;

class Bar
{
    /**
     * @var string
     */
    protected $name;

    /**
     * Bar constructor.
     *
     * @param string $name If name is empty or not a string exception is thrown
     *
     * @throws \UnexpectedValueException
     */
    public function __construct($name)
    {
        if (!is_string($name) || $name === '') {
            throw new \UnexpectedValueException('$name must be a string and not empty');
        }
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getWelcomeMessage()
    {
        return 'Hey ' . $this->name . '!';
    }
}
