<?php

namespace MelqTest\PhpTesting\Foo;

use Melq\PhpTesting\Foo\Bar;

class BarTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param string $name
     * @dataProvider provideNewInstanceThrowsExceptionWhenNameNotAStringData
     */
    public function testNewInstanceThrowsExceptionWhenNameNotAString($name)
    {
        $this->setExpectedException(\UnexpectedValueException::class);
        $bar = new Bar($name);
    }

    /**
     * @return array
     */
    public function provideNewInstanceThrowsExceptionWhenNameNotAStringData()
    {
        return [
            'name as int' => [
                1
            ],
            'name as bool' => [
                true
            ],
            'name as object' => [
                new \stdClass()
            ],
            'name as null' => [
                null
            ],
            'name as array' => [
                []
            ],
        ];
    }

    public function testNewInstanceThrowsExceptionWhenNameEmpty()
    {
        $this->setExpectedException(\UnexpectedValueException::class);
        $bar = new Bar('');
    }

    public function testGetWelcomeMessageReturnProperlyFormattedMessage()
    {
        $bar = new Bar('baz');
        $this->assertEquals('Hey baz!', $bar->getWelcomeMessage());
    }
}
